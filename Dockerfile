# Stage 1 - Build on 
FROM node:16.14 as builder
WORKDIR /usr/src/app/
ENV PATH /usr/src/app/node_modules/.bin:$PATH

COPY package.json ./
RUN npm install -g --silent

# Build app
COPY . .
RUN npm install react-scripts --silent
RUN npm run build 

# Stage 2 - Deploy on server
FROM node:16.14-slim
LABEL description="MyIot Dashboard"
LABEL maintainer="cfreire@cfreire.com.pt"
WORKDIR /root/
RUN npm install -g serve
COPY --from=builder /usr/src/app/build/ /root/build/
EXPOSE 3000
CMD ["serve", "-n", "-s" , "build"]