import React from 'react';
import './style.css';

const API = process.env.REACT_APP_REST_API;

export default class Realtime extends React.Component {

  constructor(props) {
    console.info('Starting Realtime Component')
    super(props);
    this.state = {
      data: [],
      loading: true,
    };
    setTimeout( () => this.tick(), Math.floor(Math.random() * 10))
  }

  componentDidMount() {
    this.mounted = true;
    this.intervalID = setInterval(
      () => this.tick(),
      6000 + Math.floor(Math.random() * 1000 )
    );
  }
  
  componentWillUnmount() {
    clearInterval(this.intervalID);
    this.mounted = false;
  }

  tick() {
    let query = API + "/realtime"
    fetch(query)
    .then(response => response.json())
    .then(result =>{ if (this.mounted) this.setState({ data: result, loading: false }) });
  }

  render() {
    const { data } = this.state;
    if (!data || data.length === 0) return <div></div>;
    console.log(data)
    return (<h2 data-cy="temperature">Temperature: {data.temperature} º</h2>);
  }
}