import React from 'react';
import Plot from 'react-plotly.js';

const API = process.env.REACT_APP_REST_API;

export default class App extends React.Component {

  constructor(props) {
    console.info('Starting Plot Component')
    super(props);
    this.state = {
      data: [],
      loading: true,
    };
    setTimeout( () => this.tick(), Math.floor(Math.random() * 10))
  }

  componentDidMount() {
    this.mounted = true;
    this.intervalID = setInterval(
      () => this.tick(),
      6000 + Math.floor(Math.random() * 1000 )
    );
  }
  
  componentWillUnmount() {
    clearInterval(this.intervalID);
    this.mounted = false;
  }

  tick() {
    let query = API + "/history"
    fetch(query)
    .then(response => response.json())
    .then(result =>{ if (this.mounted) this.setState({ data: result, loading: false }) });
  }

  render() {
    const { data } = this.state;
    if (!data || data.length === 0) return <div></div>;
    let timestamp = data.map(a => a.timestamp);
    let values = data.map(a => a.value);
    return (
      <Plot
        data={[
          {
            x: timestamp,
            y: values,
            type: 'scatter',
            mode: 'lines+markers',
            marker: {color: 'red'},
          },
        ]}
        layout={ {width: 800, height: 400, title: 'IoT Temperature'} }
      />
    );
  }
}

