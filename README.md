# React Dashboard Prep

Iot Temperature Sensor with cypress tests

Create a GitLab Project  
`$ git clone git@gitlab.com:form-cfreire/react-dashboard.git`

`$ npx create-react-app@latest react-dashboard --use-npm`

## Test project  
`$ npm start`

## Add dependencies

`$ npm install react-plotly.js plotly.js`

`$ npm install -D cypress`

Open and delete all tests  
Create "main.spec.js"

```js
describe('Main Tests', () => {
    it('It as title', () => {
      cy.visit('http://localhost:3000')
      cy.contains('IoT Temperature')
    })

    it('It as realtime temperature', () => {
        cy.visit('http://localhost:3000')
        cy.get('[data-cy="temperature"]').should('contain', 'Temperature')
      })
  })
  ```

Run tests  
`$ npx cypress run`

## Build 

$ docker build -t react-dashboard .

## Running

$ docker run -d -p 80:3000 --rm --name react-dashboard --link rest_server react-dashboard

## Setup CI/CD
https://docs.cypress.io/guides/continuous-integration/gitlab-ci#Caching-Dependencies-and-Build-Artifacts

Create Dockerfile  
Create .gitlab-ci.yml  
$ sudo apt install docker.io
$ sudo usermod -aG docker cfreire70
$ sudo apt install gitlab-runner