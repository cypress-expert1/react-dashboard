describe('Main Tests', () => {
  it('as title', () => {
    cy.visit('http://localhost:3000')
    cy.contains('IoT Temperature')
  })

  it('as realtime temperature', () => {
      cy.visit('http://localhost:3000')
      cy.get('[data-cy="temperature"]').should('contain', 'Temperature')
    })
})